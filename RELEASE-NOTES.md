# Release Notes

Acesse o histórico das versões publicadas e a lista de mudanças realizadas em cada uma delas, nos repositórios abaixo.

---

## Repositórios de Design

- [Diretrizes de Design](https://gitlab.com/govbr-ds/govbr-ds/-/releases)

- [Padrões Modelos](https://gitlab.com/govbr-ds/govbr-ds-design-padroes-modelos/-/releases)

- [UI-Kits](https://gitlab.com/govbr-ds/govbr-ds-design-uikits/-/releases)

---

## Repositórios de Desenvolvimento

- [Core](https://gitlab.com/govbr-ds/bibliotecas/javascript/govbr-ds-core/-/releases)

- [Web Components](https://gitlab.com/govbr-ds/bibliotecas/wc/govbr-ds-wc/-/releases)

- [React](https://gitlab.com/govbr-ds/bibliotecas/react-components/-/releases)
