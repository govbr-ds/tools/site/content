#!/usr/bin/env bash

mkdir contenttest
cd contenttest
git clone https://gitlab.com/govbr-ds/ds/dev/govbr-ds-dev-core.git
git clone https://gitlab.com/govbr-ds/ds/design/govbr-ds-design-diretrizes.git
git clone https://gitlab.com/govbr-ds/ds/dev/diretrizes-de-desenvolvimento.git
git clone https://gitlab.com/govbr-ds/tools/site/content.git

rm -f -R docs
mkdir docs
mkdir -p docs/govbr-ds-design-diretrizes/componentes
cp -r diretrizes-de-desenvolvimento/* docs
# - cp -r govbr-ds-design-diretrizes/* docs
# - cp -r govbr-ds-design-diretrizes docs
cp -r content/* docs
# https://docs-ds.estaleiro.serpro.gov.br/docs/govbr-ds-design-diretrizes/componentes/avatar/avatar.md
cp -r govbr-ds-design-diretrizes/componentes/* docs/components
cp -r govbr-ds-design-diretrizes/componentes/* docs/govbr-ds-design-diretrizes/componentes
cp -r govbr-ds-design-diretrizes/fundamentos/* docs/fundamentos-visuais
mkdir docs/padroes/design
cp -r govbr-ds-design-diretrizes/padroes/* docs/padroes/design
cp -r diretrizes-de-desenvolvimento/* docs
mkdir -p docs/ds
cd govbr-ds-dev-core
git checkout feat/remove-arquivos
npm install
npm run build
ls
cd ..
rm -f -R docs/downloads
cp govbr-ds-dev-core/RELEASE-NOTES.md docs/ds
cp -R govbr-ds-dev-core/dist docs/ds

cp govbr-ds-dev-core/package.json docs
ls
