> Utilize os UI Kits para planejar as telas do seu produto.

Precisa vender um produto, validar requisitos com o cliente, planejar a aparência do sistema que vai implementar? Um protótipo pode te ajudar muito.

A utilização do protótipo possibilita simular a aparência e funcionalidade do software, permitindo que os clientes, analistas, desenvolvedores e gerentes percebam os requisitos do sistema e possam avaliar, alterar ou aprovar as características mais marcantes na interface..

Os **UI Kits**, pacotes de interface do usuário, contém os componentes e templates do Design System GOV.BR. Com eles, você pode editar e planejar a aparência das telas do seu produto, seja um sistema web responsivo, um aplicativo para dispositivos móveis ou um site institucional.

Sendo assim, é possível validar a interface gráfica antes mesmo da implementação e ter a certeza da aparência final do seu produto.

## Requisitos e Download dos UI Kits

Os UI Kits foram criados em duas versões de ferramentas de prototipação:

- **Adobe XD** (disponível apenas para o Windows 10 e Mac OSX ou versões superiores);
- **Figma**.

### Utilizando os UI Kits

1. Acesse o site da [Adobe](https://www.adobe.com/products/xd.html) para baixar e instalar gratuitamente o Adobe XD. Ou, caso prefira, utilize o [Figma](https://www.figma.com/).
1. Logo abaixo, faça o download do UI Kit correspondente à natureza do seu produto (*Web* ou *Mobile*).
1. Abra os arquivos e siga as orientações de uso.

### UI Kit Web

Este kit contém componentes gráficos para a construção de páginas web, incluindo a paleta de cores e escala tipográfica. Os templates exemplificam e guiam a aplicação dos componentes em páginas de login, erro e formulários.

### UI Kits Mobile

São específicos para prototipação de aplicações mobile (*Android* e *IOS*). Os UI Kits Mobile disponibilizam elementos visuais, templates, paleta de cores e orientações de uso para tipografia e ícones.

<a class="br-button secondary" href="downloads/assets">Download dos UI Kits</a>

## Orientações de uso

### Adobe XD

- Certifique-se de ter o *AdobeXD* instalado em sua máquina para cumprir esta tarefa, além de estar conectado à internet. Depois de ter instalado o *AdobeXD*, faça o download do UI Kit necessário.

- Descompacte o arquivo (.zip), depois abra o *AdobeXD* e selecione a opção *“seu computador”* à direita da tela inicial. Também é possível abrir o arquivo clicando sobre ele através do *Windows Explorer* ou *Finder* no *Mac*.

- Caso ainda não saiba utilizar o *AdobeXD*, é recomendável assistir aos tutoriais disponíveis no site da própria *Adobe* [Tutoriais AdobeXD](https://helpx.adobe.com/br/support/xd.html).

- Com o UI Kit aberto, selecione as “pranchetas” contendo as páginas de exemplos que você deseja utilizar. Edite os elementos da sua prancheta ou importe outros elementos usando os “componentes” listados no menu de “ativos”. O menu de “ativos” também disponibiliza a paleta de cores e os estilos de textos da interface.

- Caso deseje tornar o protótipo dinâmico, utilize a aba protótipos para definir a navegação e/ou transições. Depois de projetar animações ou transições e quiser testá-las, basta utilizar o botão (>) play.

- Para compartilhar seu protótipo, clique no botão compartilhar, escolha o tipo de compartilhamento que deseja, e envie o link para os interessados.

- Após concluir o trabalho, salve-o.

### Figma

- Instale o aplicativo [Figma](https://www.figma.com/) em sua máquina, ou se preferir, utilize a versão online da ferramenta.

- Baixe o UI Kit para versão *Figma*, na seção [Downloads](downloads/assets) deste site.

- Descompacte o arquivo (.zip), depois abra-o através do *Figma* ou clicando sobre o arquivo através do *Windows Explorer* ou *Finder* no *Mac*.

- Caso ainda não saiba utilizar o *Figma*, é recomendável assistir aos tutoriais disponíveis no site da ferramenta - [Tutoriais Figma](https://www.figma.com/prototyping/).

- Com o UI Kit aberto, selecione as “pranchetas” contendo as páginas de exemplos que você deseja utilizar. Edite os elementos da sua prancheta ou importe outros elementos.

- Caso deseje tornar o protótipo dinâmico, utilize a aba protótipos (*Prototype*) para definir a navegação e/ou transições. Depois de projetar animações ou transições e quiser testá-las, basta utilizar o botão (>) *Present*.

- Para compartilhar seu protótipo, clique no botão compartilhar (*Share*), escolha o tipo de compartilhamento que deseja, e envie o link para os interessados.

- Após concluir o trabalho, você poderá salvar uma cópia do arquivo na sua máquina.

## Experiência do Usuário

Para o sucesso do seu produto, além de projetar uma boa interface visual, é fundamental planejar **uma boa experiência do usuário**. Saiba mais sobre o assunto nas seções *Princípios do Design System* ou *Fundamentos Visuais*.

## Ajuda e *Feedback*

Seu *feedback* é bastante valioso e necessário para evolução e aprimoramento dos Ui Kits, assim como para as melhorias nas orientações de uso.

Conte-nos o que achou do material. Caso encontre algum problema ou necessidade não contemplada, ou tenha sugestões de melhorias, entre em contato com a equipe do Design System GOV.BR.

Compartilhe conosco os protótipos que você produziu, caso tenha dúvidas.
